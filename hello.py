from textx import metamodel_from_file

hello_rules = metamodel_from_file('hello.tx')

hello_program_example = hello_rules.model_from_file('program.hello')

for recipient in hello_program_example.list_recipients:
    print(f"Hello {recipient.name}!")
